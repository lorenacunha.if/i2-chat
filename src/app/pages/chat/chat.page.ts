import { Component, OnInit } from '@angular/core';
import { Message } from 'src/app/model/message.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  chatMessages: Message[] = [
    {
      content: 'Hello',
      author: 'John',
      avatarURL:
        'https://robohash.org/9e6714c21ef7325b85e177a84bcba030?set=set4&bgset=&size=400x400',
      createdAt: new Date(),
    },
    {
      content: 'How can I help you?',
      author: 'app',
      avatarURL:
        'https://gravatar.com/avatar/699978b81748c587c2ed7027cd9e51e4?s=400&d=robohash&r=x',
      createdAt: new Date(),
    },
    {
      content: 'Explain the universe',
      author: 'John',
      avatarURL:
        'https://robohash.org/9e6714c21ef7325b85e177a84bcba030?set=set4&bgset=&size=400x400',
      createdAt: new Date(),
    },
    {
      content: 'Che-mystery',
      author: 'app',
      avatarURL:
        'https://gravatar.com/avatar/699978b81748c587c2ed7027cd9e51e4?s=400&d=robohash&r=x',
      createdAt: new Date(),
    },
  ];
  constructor() {}

  ngOnInit() {}
}
