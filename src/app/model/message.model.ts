export interface Message {
  content: string;
  author: string;
  avatarURL: string;
  createdAt: Date;
}
