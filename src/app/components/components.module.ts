import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutComponent } from './layout/layout.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { IonicModule } from '@ionic/angular';
import { MessageComponent } from './message/message.component';

@NgModule({
  imports: [CommonModule, IonicModule.forRoot()],
  declarations: [LayoutComponent, SideBarComponent, MessageComponent],
  exports: [LayoutComponent, SideBarComponent, MessageComponent],
})
export class ComponentsModule {}
