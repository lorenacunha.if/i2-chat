import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  headerTitle: string = '';

  constructor() {}

  ngOnInit() {
    console.log({ title: this.headerTitle });
    // this.headerTitle = this.headerService.headerTitle;
    console.log({ title: this.headerTitle });
  }
}
